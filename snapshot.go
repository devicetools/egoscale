package egoscale

import (
	"encoding/json"
	"net/url"
)

// ListSnapshots lists all snapshots for the given volumeId. If volumeId is empty
// lists the snapshots for all volumes
func (exo *Client) ListSnapshots(volumeId string) ([]*Snapshot, error) {

	params := url.Values{}
	if volumeId != `` {
		params.Add(`volumeid`, volumeId)
	}

	resp, err := exo.Request(`listSnapshots`, params)

	if err != nil {
		return nil, err
	}

	var r ListSnapshotsResponse

	if err := json.Unmarshal(resp, &r); err != nil {
		return nil, err
	}

	return r.Snapshots, nil
}

// CreateSnapshot cretes a snapshot for the volume with the given id
func (exo *Client) CreateSnapshot(volumeID string) (*Snapshot, error) {

	params := url.Values{}
	params.Set("volumeid", volumeID)

	resp, err := exo.Request("createSnapshot", params)

	if err != nil {
		return nil, err
	}

	var r CreateSnapshotResponse
	if err := json.Unmarshal(resp, &r); err != nil {
		return nil, err
	}

	return &r.Snapshot, nil
}

// DeleteSnapshot deletes the snapshot with the given ID
func (exo *Client) DeleteSnapshot(snapshotID string) error {
	params := url.Values{}
	params.Set("id", snapshotID)

	// This returns an answer like:
	//     {"jobid":"5401cdbf-24ce-4b75-a773-41795b5b8641"}
	_, err := exo.Request("deleteSnapshot", params)
	if err != nil {
		return err
	}

	return nil
}
