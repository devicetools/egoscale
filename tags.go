package egoscale

import (
	"net/url"
	"strings"
	"fmt"
	"encoding/json"
)

// TagResources Add tags to a list of resources. Receives the resources type and the resources ids. Return the job id
func (exo *Client) TagResources (rtype string, rids []string, tags map[string]string) (string, error) {
	// tags is a map in the form: [key : value]
	params := url.Values{}
	params.Set("resourceType", rtype)
	params.Set("resourceids", strings.Join(rids, ","))
	i := 0
	for k, v := range tags {
		params.Set(fmt.Sprintf("tags[%d].key", i), k)
		params.Set(fmt.Sprintf("tags[%d].value", i), v)
		i++
	}
	resp, err := exo.Request("createTags", params)
	if err != nil {
		return "", err
	}
	var r AddTagResponse
	if err := json.Unmarshal(resp, &r); err != nil {
		return "", err
	}
	return r.JobId, nil
}
