package egoscale

import (
	"encoding/json"
	"net/url"
)

// ListVolumes lists all Volumes
func (exo *Client) ListVolumes(vmID string) ([]*Volume, error) {

	params := url.Values{}
	if len(vmID) > 0 {
		params.Add("virtualmachineid", vmID)
	}

	resp, err := exo.Request("listVolumes", params)

	if err != nil {
		return nil, err
	}

	var r ListVolumesResponse

	if err := json.Unmarshal(resp, &r); err != nil {
		return nil, err
	}

	return r.Volumes, nil
}

//
// // CreateVolume cretes a Volume for the volume with the given id
// func (exo *Client) CreateVolume(volumeID string) (*Volume, error) {
//
// 	params := url.Values{}
// 	params.Set("volumeid", volumeID)
//
// 	resp, err := exo.Request("createShapshot", params)
//
// 	if err != nil {
// 		return nil, err
// 	}
//
// 	var r CreateVolumeResponse
// 	if err := json.Unmarshal(resp, &r); err != nil {
// 		return nil, err
// 	}
//
// 	return &r.Volume, nil
// }
